var UserName = "You dont have an account";

function newGame() {
    // reset the game
     var size=40;
    size=document.getElementById("turns").value;
    // parse game size value
    size = parseInt(size, 10);


    // check if this is a valid selection
    if (size === 0) {
        // show an error message
        alert("Please choose a size!");
    } else {
        // fetch the game board array from the server
        
        $.post("http://localhost:8090/game/start?turns=" + size, function (response) {
            // store game data * 2 for card number
            sessionStorage.clear();
            location.reload();
        });
        alert("New Market Created ");   
    }
}

function CreateUser() {
    // reset the game
    var name = $("#givenName").val();
    if(UserName=="You dont have an account"){
    // fetch the game state from the server 
    $.post("http://localhost:8090/game/createuser/"+name, function (response) {
        // store game board size
        UserName = response.name;
        sessionStorage.setItem("someVarKey", UserName);
        document.getElementById('yourName').innerHTML = UserName;
        SetBalance();
    });
}else{
    window.alert("You already have a account");
}

}

function SetBalance() {
    // reset the game
    
    // fetch the game state from the server 
    $.get("http://localhost:8090/game/balance/"+UserName, function (response) {
        // store game board size
        sessionStorage.setItem("yourbalance", response);
        document.getElementById('balance2').value = response;
        document.getElementById('playerBalance').value = sessionStorage.getItem("yourbalance");
    });

}

function SetStockBalance() {
    // reset the game
    
    // fetch the game state from the server 
    $.get("http://localhost:8090/game/stockValue/"+UserName, function (response) {
        // store game board size
        sessionStorage.setItem("yourStockbalance", response);
        document.getElementById('turn2').value = response;
        document.getElementById('playerStockBalance').value = sessionStorage.getItem("yourStockbalance");
        document.getElementById('value2').value =parseFloat(sessionStorage.getItem("yourStockbalance"))+parseFloat(sessionStorage.getItem("yourbalance"));   
        
        
        value2
    });

}

function SetEvents() {
    // reset the game
    
    // fetch the game state from the server 
    $.get("http://localhost:8090/game/currentEvents", function (response) {
        // store game board size
        var array = response;
        var color;
        var arrayLength = parseInt(array.length);
        $("#event table").empty();
        $("#event table") .append('<tr><th >EventType</th><th >Sector</th><th >Stock</th><th>StartTurn</th><th >EndTurn</th><th >value</th></tr>') 
        for (i=0; i<arrayLength; i++) {
            if(array[i].value<0){
                var color="red";
            }
            else{
                var color="green";
            }

            if(array[i].stock!=null){
                $("#event table") .append('<tr class=\"'+color+'\"><td >'+array[i].eventType+'</td><td >N/A</td><td >'+array[i].stock.companyName+'</td><td >'+array[i].startTurn+'</td><td >'+array[i].endTurn+'</td><td >'+array[i].value+'</td></tr>') 
            }else{
                $("#event table") .append('<tr class=\"'+color+'\"><td >'+array[i].eventType+'</td><td >'+array[i].sector+'</td><td >N/A </td><td >'+array[i].startTurn+'</td><td >'+array[i].endTurn+'</td><td >'+array[i].value+'</td></tr>') 
            }
          
        }
    });

}

function SetStocks() {
    
    $.get("http://localhost:8090/game/stocks", function (response) {
        
        var array = response;
        var arrayLength = parseInt(array.length);
        $("#stockTable tr").remove();
        $("#stockTable") .append('<tr><th >Company Name</th><th >Sector</th><th >Stock Price(Rs)</th></tr>') 
        for (i=0; i<arrayLength; i++) {
                $("#stockTable") .append('<tr class><td >'+array[i].companyName+'</td><td >'+array[i].sector+'</td><td >'+array[i].stockPrice+'</td></tr>');
        }
    });

    

}


function SetPredictions() {
    
    $.get("http://localhost:8090/game/prediction", function (response) {
        
        var array = response;
        document.getElementById('stockToBuy').value = array[0];
        document.getElementById('stockToSell').value = array[1];
        
    });

    

}



function SetOwnedStocks() {
    
    $.get("http://localhost:8090/game/portofolio/"+UserName, function (response) {
        
        var array = response;
       
        $("#ownedStockTable tr").remove();
        $("#ownedStockTable") .append('<tr><th >Company Name</th><th >Numbe of stocks owned</th></tr>') 

        Object.keys(array).forEach(function(key) {
            $("#ownedStockTable") .append('<tr><td >'+key+'</td><td >'+array[key]+'</td></tr>');
          })
        
    });

    

}

function SetTransactions() {
    
    $.get("http://localhost:8090/game/transactions", function (response) {
        
        var array = response;
        var arrayLength = parseInt(array.length);
        $("#TransactionTable tr").remove();
        $("#TransactionTable") .append('<tr><th >Buyer/Seller</th><th >Turn</th><th >Type</th><th >Stock Name</th><th >Quantity</th><th >Transaction Value</th></tr>') 
        for (i=0; i<arrayLength; i++) {
            if(array[i].type=="BUY"){
                var color="green";
            }
            else{
                var color="Red";
            }
                $("#TransactionTable") .append('<tr class=\"'+color+'\"><td >'+array[i].name+'</td><td >'+array[i].turn+'</td><td >'+array[i].type+'</td><td >'+array[i].stock+'</td><td >'+array[i].quantity+'</td><td >'+array[i].total+'</td></tr>');
        }
    });

}

function SetPlayers() {
    
    $.get("http://localhost:8090/game/players", function (response) {
        
        var array = response;
        var stockValue=0;
        var bankValue=0;
        $("#otherPlayers").empty();
        $("#otherPlayers") .append('<h2>Other Players</h2>')
       
        Object.keys(array).forEach(function(key) {
            if(key==UserName){
                
            }
            else{
                var total=parseFloat(array[key][1])+parseFloat(array[key][0]);
                $("#otherPlayers") .append('<div class="player-box"><h3 id="playerName" class="hedding-tertiary">'+key+'</h3><div class="balance"><label for="balance1">balance :</label><input type="text" id="balance1" class="player-box--value" Value="'+array[key][1]+'"></div>'
                +'<div class="turn"><label for="turn1">Stock Value :</label><input type="text" id="turn1" class="player-box--value" Value="'+array[key][0]+'"></div>'
                +'<div class="value"><label for="value1">Net Value:</label><input type="text" id="value1" class="player-box--value" Value="'+total+'"></div>'
                +'</div>');
                
            }
          })
    });

}

function SetWinner() {
    
    $.get("http://localhost:8090/game/winner", function (response) {
        
        
        document.getElementById('winner').innerHTML=response.name;
        
    });

}

function SetTurn() {
    
    $.get("http://localhost:8090/game/currentTurn", function (response) {
        
        
        document.getElementById('turnLabel').innerHTML="Current Turn: "+response;
        
    });

}

function BuyStocks() {
    
    var stock=$("#stockTable tr.selectedStock td:first").html();
    var quantity=$("#buyQuantity").val();
    if(UserName!="You dont have an account"){
    if(stock!=null){
    $.post("http://localhost:8090/game/buyStock/"+UserName+"?stock="+stock+"&quantity="+quantity, function (response) {
        
      if(response==true) {
        alert("Success");
      } 
      else{
        alert("Insufficient founds");
      }
        
    });
    }
    else{
        alert("Please select a stock");
    }
    }
    else{
        alert("Please create a account before buying any stock");
    }
}

function SellStocks() {
    
    var stock=$("#ownedStockTable tr.selectedStock td:first").html();
    var quantity=$("#sellQuantity").val();
    if(UserName!="You dont have an account"){
    if(stock!=null){
    $.post("http://localhost:8090/game/sellStock/"+UserName+"?stock="+stock+"&quantity="+quantity, function (response) {
        
      if(response==true) {
        alert("Success");
      } 
      else{
        alert("you dont own that many stocks");
      }
        
    });
    }
    else{
        alert("Please select a stock");
    }
    }
    else{
        alert("Please create a account before buying any stock");
    }
}





 $(document).on("click", "#stockTable tr", function(e) {
    $(this).addClass('selectedStock').siblings().removeClass('selectedStock');    
    var value=$(this).find('td:first').html();
});

$(document).on("click", "#ownedStockTable tr", function(e) {
    $(this).addClass('selectedStock').siblings().removeClass('selectedStock');    
    var value=$(this).find('td:first').html();
});

window.onload = function(){
    UserName=sessionStorage.getItem("someVarKey");
    
    if(UserName==null||UserName=="undefined"){
        UserName = "You dont have an account";
    }
    SetBalance();
    SetEvents();
    SetOwnedStocks();SetStockBalance();SetStocks();
    SetWinner();
    document.getElementById('balance2').value = sessionStorage.getItem("yourbalance");
    document.getElementById('yourName').innerHTML = UserName;
    SetPlayers()
    SetTurn()
};




function RefreshFunctions() {
    SetBalance();
    SetEvents();
    SetOwnedStocks();SetStockBalance();SetStocks();
    SetWinner();
    SetPlayers()
    SetPredictions();
    SetTurn()
}

setInterval(RefreshFunctions, 15000); 



