package main;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.internal.Throwables;

public class Bank {
	List<Account> accountList;
	
	public Bank() {
		accountList = new ArrayList<>();
	}
	
	//create new account 
	public void CreateAccount(String name) throws Exception {
		for (Account account : accountList) {
		    if (name.equals(account.getName())) {
		        throw new Exception("account with "+name+" already exists");
		    }
		}
		Account newAccount =new Account(name);
		accountList.add(newAccount);
				
	}
	
	//deposit to an account
	public Boolean Deposit(int turn,String name,String sender,BigDecimal amount) throws Exception {
		for (Account account : accountList) {
		    if (name.equals(account.getName())) {
		    	account.setBalance(account.getBalance().add(amount));
		        return true;
		    }
		}
		throw new Exception("account with "+name+" does not  exists");
	}
	
	//withdraw from account
	public Boolean Withdraw(int turn,String name,String receiver,BigDecimal amount) throws Exception {
		for (Account account : accountList) {
		    if (name.equals(account.getName())) {
		    	if (amount.compareTo(account.getBalance())<=0) {
		    		account.setBalance(account.getBalance().subtract(amount));
		    		return true;
				}
		    	else {
					return false;
				}
		    }
		}
		throw new Exception("account with "+name+" does not  exists");
	}
	
	//return balance
	public BigDecimal Balance(String name) throws Exception {
		for (Account account : accountList) {
		    if (name.equals(account.getName())) {
		    	return account.getBalance();
		    }
		}
		throw new Exception("account with "+name+" does not  exists");
	}

}
