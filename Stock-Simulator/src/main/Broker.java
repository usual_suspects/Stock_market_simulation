package main;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Broker {
	Market market;
	Bank bank;
	public ArrayList<Player> stockAccounts;
	public ArrayList<String> Transactions;
	
	//initialize a broker
	public Broker() {
		
	}
	public Broker(Market market,Bank bank) {
		this.market=market;
		this.bank=bank;
		
	}
	//create stock account along with bank account
	public void CreateAccount(String name) {		
		try {
			bank.CreateAccount(name);
			Player newPlayer=new Player(name);
			stockAccounts.add(newPlayer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//buy stock
	public Boolean Buy(String name,String stock,int quantity) {
		try {
				BigDecimal totalvalue=market.getStock(stock).getStockPrice().multiply(BigDecimal.valueOf(quantity));	
				if (bank.Balance(name).compareTo(totalvalue)<=0) {	
					Integer count = GetPlayer("name").stocks.get(stock);
					if (count == null) {
						GetPlayer(name).stocks.put(stock, quantity);
					}
					else {
						GetPlayer(name).stocks.put(stock, count + quantity);
					}
					bank.Withdraw(market.GetCurrentTurn(), name, stock, totalvalue);
					Transactions.add(market.GetCurrentTurn()+","+"BUY"+","+stock+","+quantity+","+totalvalue);
					return true;
				}
				else {
					return false;
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	//sell stock
	public Boolean Sell(String name,String stock,int quantity) {
		try {
			Integer stockCount=GetPlayer(name).stocks.get(stock);
			if (stockCount!=null && stockCount>=quantity) {
				GetPlayer(name).stocks.put(stock, stockCount+quantity);
				BigDecimal totalvalue=market.getStock(stock).getStockPrice().multiply(BigDecimal.valueOf(quantity));
				bank.Deposit(market.GetCurrentTurn(), name, stock, totalvalue);
				Transactions.add(market.GetCurrentTurn()+","+"SELL"+","+stock+","+quantity+","+totalvalue);
				return true;
			}
			else
				return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	//get player buy name
		public Player GetPlayer(String name) throws Exception {

		    for (Player c : stockAccounts) {
		        if (name.equals(c.name)) {
		            return c;
		        }
		    }
		    throw new Exception("Player with the name "+name+" does not exsist");
			
		}
	
	

}
