package main;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Market {
	public ArrayList<Stock> stocks;
	public HashMap<Sector, ArrayList<Integer>> sectorTrends;
	public ArrayList<Integer> marketTrends;
	public ArrayList<Event> eventList;
	private int turns=20;
	private int currentTurn=0;
	
	public Market() {
		 stocks=new ArrayList<>();
		 eventList=new ArrayList<>();
		 marketTrends= new ArrayList<>();
		 sectorTrends=new HashMap<>();
		 //add stocks
		 stocks.add(new Stock("Apple",Sector.Technology,new BigDecimal(58)));
		 stocks.add(new Stock("Microsoft",Sector.Technology,new BigDecimal(44.5)));
		 stocks.add(new Stock("Apple",Sector.Technology,new BigDecimal(75.5)));
		 
		 stocks.add(new Stock("Allianz",Sector.Finance,new BigDecimal(77.5)));
		 stocks.add(new Stock("ICBC",Sector.Finance,new BigDecimal(56.5)));
		 stocks.add(new Stock("AXA",Sector.Finance,new BigDecimal(55)));
		 
		 stocks.add(new Stock("SERV",Sector.ConsumerServices,new BigDecimal(76.5)));
		 stocks.add(new Stock("BID",Sector.ConsumerServices,new BigDecimal(45.5)));
		 stocks.add(new Stock("HRB",Sector.ConsumerServices,new BigDecimal(25)));
		 
		 stocks.add(new Stock("Volkswagen",Sector.Manufacturing,new BigDecimal(55)));
		 stocks.add(new Stock("Samsung",Sector.Manufacturing,new BigDecimal(75)));
		 stocks.add(new Stock("Daimler",Sector.Manufacturing,new BigDecimal(58)));
		 //set trends
		 setEvents();
		 setTrends();
		 ChangeStockValues();
	}
	//set Events for every turn
	public void setEvents() {
		int probability=0;
		Random random = new Random();
		int nextInt;
		//loop for number of turns in the game 
		for(int i=0;i<this.turns;i++) {
			nextInt = random.nextInt(10);
			if (nextInt < probability) {
				Event evnt=new Event(stocks,i);
				eventList.add(evnt);
				probability=0;
			}
			probability++;
			
		}
	}
	public void setTrends() {
		//set market Trends
		Random random = new Random();
		Random randomvalue = new Random();
		int nextInt;
		for(int i=0;i<this.turns;i++) {
			nextInt = random.nextInt(100);
			if (nextInt < 50) {
				if (nextInt < 25) {
					marketTrends.add(randomvalue.nextInt(0 + 1 + 0) - 5);
				}else {
					marketTrends.add(randomvalue.nextInt(5));
				}
				
			}else {
				marketTrends.add(0);
			}
			
		}
		
		
		//set sector trends
		for (Sector d : Sector.values()) {
			ArrayList<Integer> trends=new ArrayList();
			for(int i=0;i<this.turns;i++) {			
				nextInt = random.nextInt(100);
				if (nextInt < 50) {
					if (nextInt < 25) {
						trends.add(randomvalue.nextInt(0 + 1 + 0) - 5);
					}else {
						trends.add(randomvalue.nextInt(5));
					}
					
				}else {
					trends.add(0);
				}
				
			}
			sectorTrends.put(d, trends);
			
		     
		 }
		
	}
	//
	public void NextTurn() {
		this.currentTurn++;	
			ChangeStockValues();
	}
	
	//change stock value based on event market and sector trends
	public void ChangeStockValues() {
		for (int i = 0; i < stocks.size(); i++) {
			//get market trend value for current turn
			int marketValue=marketTrends.get(currentTurn);
			//get sector trend value value for current turn
			int sectorValue=sectorTrends.get(stocks.get(i).getSector()).get(currentTurn);
			int eventValue=ChangeEventStockValue(stocks.get(i),currentTurn);
			BigDecimal stockprice=stocks.get(i).getStockPrice();
			stocks.get(i).setStockPrice(stockprice.add(BigDecimal.valueOf(eventValue+sectorValue+marketValue)));
			
		}
	}
	//change stock value based on events
	public int ChangeEventStockValue(Stock stock,int turn){
		int eventValue=0;
		//loop all events
		for(int i=0;i<eventList.size();i++) {
			if ((eventList.get(i).sector==stock.getSector())||(eventList.get(i).stock==stock)) {
				if (turn >= eventList.get(i).startTurn && turn <= eventList.get(i).endTurn) {
					eventValue=eventValue+eventList.get(i).value;
				}
				
			}
		}

		return eventValue;
	}
	//get current events
	public ArrayList<Event> GetCurrentEvents(){
		ArrayList<Event> eList=new ArrayList<>();
		for(int i=0;i<eventList.size();i++) {
			if (this.currentTurn >= eventList.get(i).startTurn && this.currentTurn <= eventList.get(i).endTurn) {
				eList.add(eventList.get(i));
			}
		}
		return eList;
	}
	//get stock buy name
	public Stock getStock(String name) throws Exception {

	    for (Stock c : stocks) {
	        if (name.equals(c.getCompanyName())) {
	            return c;
	        }
	    }
	    throw new Exception("Stock with the name "+name+" does not exsist");
		
	}
	//get current turn
	public int GetCurrentTurn() {
		return this.currentTurn;
	}
	
}
