package main;

import java.math.BigDecimal;

public class Stock {
	private int stockId;
	private BigDecimal stockPrice;
	private String companyName;
	private Sector sector;
	private static int stockIdCounter = 0;
	
	
	
	public Stock(String companyName, Sector sector,BigDecimal stockPrice) {
		super();
		this.stockId = stockIdCounter++;;
		this.stockPrice = stockPrice;
		this.companyName = companyName;
		this.sector = sector;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public long getStockId() {
		return stockId;
	}
	public BigDecimal getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(BigDecimal stockPrice) {
		this.stockPrice = stockPrice;
	}
	public Sector getSector() {
		return sector;
	}
	public void setSector(Sector sector) {
		this.sector = sector;
	}
	

}
