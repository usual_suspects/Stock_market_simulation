package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.math.BigDecimal;

import org.junit.Test;

import main.Bank;

public class BankTest {
	Bank bank =new Bank();
	
	@Test 
	public void deposit() {
		try {
			bank.CreateAccount("cake");
			bank.CreateAccount("apple");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			bank.Deposit(1,"cake", "bank", BigDecimal.valueOf(250));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			org.junit.Assert.assertEquals(bank.Balance("cake"),BigDecimal.valueOf(1250));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void depositFail() {
		try {
			bank.CreateAccount("cake");
			bank.CreateAccount("apple");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			bank.Deposit(1,"cake1", "bank", BigDecimal.valueOf(250));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("account with cake1 does not  exists", e.getMessage());
		}
		
		
	}
	
	@Test
	public void withdraw() {
		try {
			bank.CreateAccount("cake");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			boolean ans;
			ans=bank.Withdraw(1,"cake", "bank", BigDecimal.valueOf(250));
			assertSame(true,ans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("account with cake does not  exists", e.getMessage());
		}
		try {
			assertEquals(BigDecimal.valueOf(750),bank.Balance("cake"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
