package com.usualSuspects.stockMarket.controller;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.usualSuspects.stockMarket.dao.*;
import com.usualSuspects.stockMarket.exceptions.UserAlreadyAvailableException;
import com.usualSuspects.stockMarket.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class MainRESTController {
 
    @Autowired
    private GameDAO gameDAO;
 
    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to Stock market Simulator API.";
    }
    
    //start a game
    // http://localhost:8080/SomeContextPath/game/start/
    @RequestMapping(value = "/game/start", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public Broker createUser(@RequestParam(value="turns", required=true) int turns)throws Exception {
    	 System.out.println("(Service Side) Game Started with turns: " + turns);
        return gameDAO.StartGame(turns);
       

        	       
    }
    
    //create a user
    // http://localhost:8080/SomeContextPath/game/createUser/{username}
    @RequestMapping(value = "/game/createuser/{username}", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public Player createUser(@PathVariable("username") String username)throws UserAlreadyAvailableException {
    	 System.out.println("(Service Side) Created user: " + username);
        return gameDAO.CreateUser(username);
       

        	       
    }
    
    //Buy Stock
    // http://localhost:8080/SomeContextPath/game/buyStock/{username}
    @RequestMapping(value = "/game/buyStock/{username}", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public boolean buyStock(@PathVariable("username") String username,@RequestParam(value="stock", required=true) String stock,
    	    @RequestParam(value="quantity", required=true) int quantity)throws Exception {
    	System.out.println("(Service Side)"+ username+" bought "+quantity+" "+stock+" stocks");
        return gameDAO.BuyStock(username, stock, quantity);
        
    }
    
    //Sell Stock
    // http://localhost:8080/SomeContextPath/game/sellStock/{username}
    @RequestMapping(value = "/game/sellStock/{username}", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public boolean sellStock(@PathVariable("username") String username,@RequestParam(value="stock", required=true) String stock,
    	    @RequestParam(value="quantity", required=true) int quantity)throws Exception {
    	System.out.println("(Service Side)"+ username+" sold "+quantity+" "+stock+" stocks");
        return gameDAO.SellStock(username, stock, quantity);
        
    }
    
    //get all Stocks
    // http://localhost:8080/SomeContextPath/game/stocks/
    @RequestMapping(value = "/game/stocks", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ArrayList<Stock> getStocks()throws Exception {
    	
        return gameDAO.getStocks();
        
    }
    
    //get player Portofolio
    // http://localhost:8080/SomeContextPath/game/portofolio/{username}
    @RequestMapping(value = "/game/portofolio/{username}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  HashMap<String, Integer> getportofolio(@PathVariable("username") String username)throws Exception {
    	
        return gameDAO.getPortofolio(username);
        
    }
    
    //get player total stock value
    // http://localhost:8080/SomeContextPath/game/stockValue/{username}
    @RequestMapping(value = "/game/stockValue/{username}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  BigDecimal stockValue(@PathVariable("username") String username)throws Exception {
    	
        return gameDAO.getStockValue(username);
        
    }
    
    //get player total Bank Balance
    // http://localhost:8080/SomeContextPath/game/balance/{username}
    @RequestMapping(value = "/game/balance/{username}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  BigDecimal balance(@PathVariable("username") String username)throws Exception {
    	
        return gameDAO.getBankBalance(username);
        
    }
    
    //get all Transactions 
    // http://localhost:8080/SomeContextPath/game/transactions
    @RequestMapping(value = "/game/transactions", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  ArrayList<Transaction> transactions()throws Exception {
    	
        return gameDAO.getTransactions();
        
    }
    
    //get all Transactions 
    // http://localhost:8080/SomeContextPath/game/currentEvents
    @RequestMapping(value = "/game/currentEvents", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  ArrayList<Event> currentEvents()throws Exception {
    	
        return gameDAO.getCurrentEvents();
        
    }
    
  //get all Transactions 
    // http://localhost:8080/SomeContextPath/game/winner
    @RequestMapping(value = "/game/winner", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  Player winner()throws Exception {
    	
        return gameDAO.getWinner();
        
    }
    
    //get all Players 
    // http://localhost:8080/SomeContextPath/game/players
    @RequestMapping(value = "/game/players", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public  HashMap<String, ArrayList<BigDecimal>> players()throws Exception {
    	
        return gameDAO.getAllPlayers();
        
    }
    
    
    
    //go to next turn
    // http://localhost:8080/SomeContextPath/game/nextTurn
    @RequestMapping(value = "/game/nextTurn", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public void nextTurn() throws Exception {
 
        System.out.println("(Service Side) Next turn");
 
        gameDAO.NextTurn();
        
    }
    //go to next turn after 45 seconds
    @Scheduled(fixedDelay=35000)
    public int nextTurnS()throws Exception {
    	 
        System.out.println("(Service Side) Next turn");
 
        return gameDAO.NextTurn();
        
    }
    
    //go to next turn
    // http://localhost:8090/SomeContextPath/game/currentTurn
    @RequestMapping(value = "/game/currentTurn", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public int currentTurn() {
 
 
        return gameDAO.CurrentTurn();
        
    }
    
    //go to next turn
    // http://localhost:8090/SomeContextPath/game/prediction
    @RequestMapping(value = "/game/prediction", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ArrayList<String> Prediction()throws Exception {
 
 
        return gameDAO.getPredictions();
        
    }
    
     
}
