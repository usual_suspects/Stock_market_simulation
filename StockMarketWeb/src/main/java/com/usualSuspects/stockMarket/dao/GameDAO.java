package com.usualSuspects.stockMarket.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.usualSuspects.stockMarket.exceptions.UserAlreadyAvailableException;
import com.usualSuspects.stockMarket.model.*;


import org.springframework.stereotype.Repository;
 
@Repository
public class GameDAO {
	Market market;
	Bank bank;
	Broker broker;
  
 
    static {
        initEmps();
    }
 
    private static void initEmps() {

    }
    
    public Broker StartGame(int turns) {
    	market=new Market(turns);
    	bank=new Bank();
    	broker =new Broker(market, bank);
    	return broker;
    }
    
    public Player CreateUser(String empName)throws UserAlreadyAvailableException {
    	return broker.CreateAccount(empName);
    }
    
    public int NextTurn() throws Exception {
    	if(market.GetCurrentTurn()<market.turns) {
    		broker.ComputerPlay();
    	return market.NextTurn();
    	}
    	return market.GetCurrentTurn();
    }
    
    public int CurrentTurn() {
    	return market.GetCurrentTurn();
    }
    
    public boolean BuyStock(String name,String stock,int quantity)throws Exception {
    	return broker.Buy(name, stock, quantity);
    }
    
    public boolean SellStock(String name,String stock,int quantity)throws Exception {
    	return broker.Sell(name, stock, quantity);
    }
    
    public ArrayList<Stock> getStocks()throws Exception {
    	return market.getStocks();
    }
    
    public HashMap<String, Integer> getPortofolio(String name)throws Exception {
    	return broker.GetPlayer(name).GetPortofolio();
    }
    
    public BigDecimal getStockValue(String name)throws Exception {
    	return broker.GetStockValue(name);
    }
 
    public BigDecimal getBankBalance(String name)throws Exception {
    	return bank.Balance(name);
    }
    
    public ArrayList<Transaction> getTransactions()throws Exception {
    	return broker.Transactions;
    }
    
    public ArrayList<Event> getCurrentEvents()throws Exception{
    	return market.GetCurrentEvents();
    }
    
    public Player getWinner()throws Exception{
    	return broker.GetWinner();
    }
    
    public HashMap<String, ArrayList<BigDecimal>> getAllPlayers()throws Exception{
    	return broker.GetAllPlayers();
    }
    
    public ArrayList<String> getPredictions()throws Exception{
    	return broker.Prediction();
    }


 
}
