package com.usualSuspects.stockMarket.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.CONFLICT,reason="This user name is already Taken")
public class UserAlreadyAvailableException extends Exception {
	private static final long serialVersionUID = 100L;
}
