package com.usualSuspects.stockMarket.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.usualSuspects.stockMarket.exceptions.UserAlreadyAvailableException;

public class Broker {
	Market market;
	Bank bank;
	public ArrayList<Player> stockAccounts;
	public ArrayList<Transaction> Transactions;
	
	//initialize a broker
	public Broker(Market market,Bank bank) {
		this.market=market;
		this.bank=bank;
		stockAccounts=new ArrayList<>();
		Transactions= new ArrayList<>();
		try {
			CreateAccount("Computer");
		} catch (UserAlreadyAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//create stock account along with bank account
	public Player CreateAccount(String name)throws UserAlreadyAvailableException {		
			bank.CreateAccount(name);
			Player newPlayer=new Player(name);
			stockAccounts.add(newPlayer);
			return newPlayer;
		
	}
	
	//buy stock
	public Boolean Buy(String name,String stock,int quantity) throws Exception{

				BigDecimal totalvalue=market.getStock(stock).getStockPrice().multiply(BigDecimal.valueOf(quantity));	
				if (bank.Balance(name).compareTo(totalvalue)>=0) {	
					Integer count = GetPlayer(name).stocks.get(stock);;
					if (count == null) {
						GetPlayer(name).stocks.put(stock, quantity);
					}
					else {
						GetPlayer(name).stocks.put(stock, count + quantity);
					}
					bank.Withdraw(market.GetCurrentTurn(), name, stock, totalvalue);
					Transactions.add(new Transaction(name,market.GetCurrentTurn(),"BUY",stock,quantity,totalvalue));
					return true;
				}
				else {
					return false;
				}
		
		
	}
	//sell stock
	public Boolean Sell(String name,String stock,int quantity)throws Exception {
			Integer stockCount=GetPlayer(name).stocks.get(stock);
			if (stockCount!=null && stockCount>=quantity) {
				GetPlayer(name).stocks.put(stock, stockCount-quantity);
				BigDecimal totalvalue=market.getStock(stock).getStockPrice().multiply(BigDecimal.valueOf(quantity));
				bank.Deposit(market.GetCurrentTurn(), name, stock, totalvalue);
				Transactions.add(new Transaction(name,market.GetCurrentTurn(),"SELL",stock,quantity,totalvalue));
				return true;
			}
			else
				return false;
		
	}
	
	
	
	//get player buy name
		public Player GetPlayer(String name) throws Exception {

		    for (Player c : stockAccounts) {
		        if (name.equals(c.name)) {
		            return c;
		        }
		    }
		    throw new Exception("Player with the name "+name+" does not exsist");
			
		}
		
		//get all Players
		public HashMap<String,ArrayList<BigDecimal>>GetAllPlayers() throws Exception{
			HashMap<String, ArrayList<BigDecimal>> hashmap=new HashMap();
			 for (Player c : stockAccounts) {
			        ArrayList<BigDecimal> al=new ArrayList<>();
			        al.add(GetStockValue(c.name));
			        al.add(bank.Balance(c.name));
			        hashmap.put(c.name, al);
			    }
			 return hashmap;
			    
		}
		
		//get stock value
		public BigDecimal GetStockValue(String name) throws Exception {
			HashMap<String,Integer>stocks=GetPlayer(name).GetPortofolio();
			BigDecimal total=BigDecimal.valueOf(0);
			for (Map.Entry<String, Integer> entry : stocks.entrySet()) {
			   total=total.add(market.getStock(entry.getKey()).getStockPrice().multiply(BigDecimal.valueOf(entry.getValue())));
			}
			return total;
		}
		
		
		//get current winner name
				public Player GetWinner() throws Exception {
					Player winner=stockAccounts.get(0);
				    for (Player c : stockAccounts) {
				    	BigDecimal value=GetStockValue(c.name).add(bank.Balance(c.name));
				    	BigDecimal Winnervalue=GetStockValue(winner.name).add(bank.Balance(winner.name));
				    	if(value.compareTo(Winnervalue)>0)
				        {
				            winner=c;
				        }
				    }
				    return winner;
					
				}
				
				
				//return min and max stock in future or current turn
				public ArrayList<String> Prediction() {
					ArrayList<Stock> futureStocks=market.GetPredictedStocks();
					String max=futureStocks.get(12).getCompanyName();
					BigDecimal valMax=futureStocks.get(12).getStockPrice();
					String min=futureStocks.get(12).getCompanyName();
					BigDecimal valMin=futureStocks.get(12).getStockPrice();
					for (int i = 12; i < futureStocks.size(); i++) {
						if (futureStocks.get(i).getStockPrice().compareTo(valMax)>0) {
							max=futureStocks.get(i-12).getCompanyName();
							valMax=futureStocks.get(i).getStockPrice();
							
						}
						
					}
					for (int i = 12; i < futureStocks.size(); i++) {
						if (futureStocks.get(i).getStockPrice().compareTo(valMin)<0) {
							min=futureStocks.get(i-12).getCompanyName();
							valMin=futureStocks.get(i).getStockPrice();
							
						}
						
					}
					ArrayList<String>prediction=new ArrayList<>();
					prediction.add(max);
					prediction.add(min);
					return prediction;
					
				}
				
				//computer play
				public void ComputerPlay() throws Exception {
					ArrayList<String> predictions=this.Prediction();
					Random ran = new Random();
					int number = ran.nextInt(7 - 1 + 1) + 1;
						if(bank.Balance("Computer").compareTo(market.getStock(predictions.get(0)).getStockPrice().multiply(BigDecimal.valueOf(number)))>0) {
							this.Buy("Computer", predictions.get(0), number);
						}
					
						Player p =GetPlayer("Computer");
						
						if(p.stocks.get(predictions.get(1))!=null) {
						Sell("Computer", predictions.get(1), p.stocks.get(predictions.get(1)));
						}else if(bank.Balance("Computer").compareTo(BigDecimal.valueOf(500))<0) {
							
							for (Map.Entry<String, Integer> entry : p.stocks.entrySet()) {
							    String key = entry.getKey();
							    int value = entry.getValue();
							    if(value>0) {
							    	Sell("Computer", key, value);
							    	break;
							    }
							}
						}
						
					

				}
		
	
	

}
