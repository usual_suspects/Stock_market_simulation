package com.usualSuspects.stockMarket.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.usualSuspects.stockMarket.exceptions.UserAlreadyAvailableException;
import com.usualSuspects.stockMarket.model.Bank;
import com.usualSuspects.stockMarket.model.Broker;
import com.usualSuspects.stockMarket.model.Market;



public class GameTest {
	
	Broker broker;
	@Test
	public void StartGame() {
		Market market=new Market(20);
		Bank bank=new Bank();
		this.broker =new Broker(market, bank);
		try {
			this.broker.CreateAccount("cake");
			this.broker.CreateAccount("apple");
		} catch (UserAlreadyAvailableException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		market.NextTurn();
		try {
			assertTrue(broker.Buy("cake", "Apple", 5));
			assertFalse(broker.Buy("cake", "Apple", 500));
			market.NextTurn();
			assertTrue(broker.Sell("cake", "Apple", 5));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		try {
			broker.GetPlayer("cake").GetPortofolio();
			System.out.println(bank.Balance("cake"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	

}
